import 'package:flutter/material.dart';
import 'package:lonux_inventory_scanner/auth/Login.dart';
import 'package:lonux_inventory_scanner/screens/Home.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Wrapper extends StatefulWidget {
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  bool isLoggedIn = false;
  _routeUser() async {
    var localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    if (token == null) {
      setState(() {
        isLoggedIn = false;
      });
    } else {
      setState(() {
        isLoggedIn = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _routeUser();
  }

  Widget build(BuildContext context) {
    return isLoggedIn ? Home() : Login();
  }
}
