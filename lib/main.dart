import 'package:flutter/material.dart';
import 'package:lonux_inventory_scanner/Wrapper.dart';
import 'package:lonux_inventory_scanner/auth/Login.dart';
import 'package:lonux_inventory_scanner/screens/AddItem.dart';

void main() {
  runApp(LonuxInventory());
}

class LonuxInventory extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lonux Inventory Scanner',
      theme: ThemeData(
        fontFamily: 'Montserrat',
        primarySwatch: Colors.grey,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => Wrapper(),
        '/add': (context) => AddItem(),
        '/login': (context) => Login(),
      },
    );
  }
}
