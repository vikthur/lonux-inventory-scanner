import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lonux_inventory_scanner/models/ImageModel.dart';

class ImageCard extends StatefulWidget {
  dynamic imageObj;
  final VoidCallback delete;

  ImageCard({required this.imageObj, required this.delete});

  @override
  _ImageCardState createState() => _ImageCardState();
}

class _ImageCardState extends State<ImageCard> {
  final ImagePicker _picker = ImagePicker();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // PickedFile? _imageFile;

  void takePhoto(ImageSource source) async {
    final pickedFile = await _picker.getImage(source: source);

    ImageModel newFileObj = ImageModel(imageType: 'new', imageSrc: pickedFile);
    setState(() {
      widget.imageObj = newFileObj;
      // _imageFile = pickedFile!;
      // widget.imageSrc = await _imageFile!.readAsBytes();
    });

    // Navigator.pop(context);
  }

  Widget bottomPop(context) {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Stack(children: [
        Positioned(
          child: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.close,
              color: Colors.red,
            ),
          ),
          top: 0,
          right: 0,
        ),
        Column(
          children: [
            Text(
              'Choose image',
              style: TextStyle(fontSize: 20, fontFamily: 'MontserratMedium'),
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton.icon(
                    onPressed: () => {takePhoto(ImageSource.camera)},
                    icon: Icon(Icons.camera_alt),
                    label: Text(
                      'Camera',
                      style: TextStyle(
                          fontFamily: 'MontserratMedium',
                          fontWeight: FontWeight.bold),
                    )),
                TextButton.icon(
                    onPressed: () => {takePhoto(ImageSource.gallery)},
                    icon: Icon(Icons.image),
                    label: Text('Gallery',
                        style: TextStyle(
                            fontFamily: 'MontserratMedium',
                            fontWeight: FontWeight.bold)))
              ],
            )
          ],
        ),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Stack(children: [
          if (widget.imageObj.imageType == 'new')
            Image.file(File(widget.imageObj.imageSrc.path))
          else if (widget.imageObj.imageType == 'placeholder')
            Image.asset(widget.imageObj.imageSrc)
          else
            Image.network(
              widget.imageObj.imageSrc,
              fit: BoxFit.fill,
            ),
          Positioned(
            child: InkWell(
              onTap: () {
                showBottomSheet(
                    context: context,
                    builder: ((builder) => bottomPop(context)));
              },
              child: Icon(
                Icons.edit,
                color: Color(0XFFe58f0d),
              ),
            ),
            bottom: 20,
            left: 20,
          ),
          Positioned(
            child: InkWell(
              onTap: widget.delete,
              child: Icon(
                Icons.delete_forever,
                color: Colors.red,
              ),
            ),
            bottom: 20,
            right: 20,
          )
        ]),
      ),
    );
  }
}
