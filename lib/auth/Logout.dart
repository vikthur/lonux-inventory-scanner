import 'package:shared_preferences/shared_preferences.dart';

class Logout {
  logout() async {
    var localStorage = await SharedPreferences.getInstance();
    localStorage.remove('user');
    localStorage.remove('token');
    localStorage.remove('shop');
  }
}
