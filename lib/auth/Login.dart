import 'dart:convert';

import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:lonux_inventory_scanner/api/CallLonux.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var phoneNumber, password;
  bool isVisible = true, isLoggingIn = false;
  Map args = {};

  final _formKey = GlobalKey<FormState>();

  void _login() async {
    setState(() {
      isLoggingIn = true;
    });
    var data = {'phone': phoneNumber, 'password': password};
    var res = await CallLonux().postData('mlogin', data);
    var body = jsonDecode(res.body);
    // print(body['data']);
    if (res.statusCode == 200 && body['success']) {
      var localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['data']['token']);
      localStorage.setString('user', jsonEncode(body['data']['user']));
      localStorage.setString('shop', jsonEncode(body['data']['shop']));
    }
    setState(() {
      isLoggingIn = false;
    });

    print(body);

    Navigator.pushReplacementNamed(context, '/');
  }

  @override
  Widget build(BuildContext context) {
    // args = ModalRoute.of(context)!.settings.arguments as Map;
    // if (args['mes'] != null) {
    //   SnackBar(content: Text(args['mes']));
    // }
    return Scaffold(
        // resizeToAvoidBottomInset: false,
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.grey[200],
          title: Image.asset(
            'assets/images/lonux-logo-01.png',
            width: 130.0,
          ),
          elevation: 0.0,
        ),
        body: DoubleBackToCloseApp(
          snackBar: const SnackBar(
            content: Text('Tab back again to exit app'),
          ),
          child: SingleChildScrollView(
            child: SafeArea(
                child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 120, 0.0, 0.0),
                child: Column(
                  children: [
                    Text(
                      'Welcome, shop manager',
                      style: TextStyle(
                        fontSize: 36.0,
                        fontFamily: 'Montserrat',
                        letterSpacing: 2.0,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      'Please sign in',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 40.0),
                    Form(
                      key: _formKey,
                      // ignore: deprecated_member_use
                      // autovalidate: true,
                      child: Padding(
                        padding:
                            const EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
                        child: Column(
                          children: [
                            // Text(
                            //   'Phone Number',
                            //   style: TextStyle(fontWeight: FontWeight.bold),
                            //   textAlign: TextAlign.left,
                            // ),
                            TextFormField(
                              maxLength: 10,
                              validator: (value) {
                                if (value == null) {
                                  return 'Please enter phone number';
                                } else if (value.length != 10) {
                                  return 'Invalid phone number';
                                }
                              },
                              onChanged: (value) {
                                setState(() {
                                  phoneNumber = '234' + value;
                                });
                              },
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14.4,
                                  height: 2.0),
                              decoration: InputDecoration(
                                  prefix: Text('234'),
                                  prefixStyle:
                                      TextStyle(fontWeight: FontWeight.bold),
                                  filled: true,
                                  fillColor: Colors.white,
                                  contentPadding: const EdgeInsets.only(
                                      left: 16.0,
                                      bottom: 8.0,
                                      top: 8.0,
                                      right: 16.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(0XFFdee2e6),
                                          style: BorderStyle.solid,
                                          width: 1.0)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(0XFFfed098),
                                          style: BorderStyle.solid,
                                          width: 2.0)),
                                  errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.0, color: Colors.red)),
                                  errorStyle:
                                      TextStyle(fontWeight: FontWeight.bold),
                                  focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.0, color: Colors.red)),
                                  labelText: 'phone number'),
                              keyboardType: TextInputType.phone,
                            ),
                            SizedBox(height: 30.0),
                            TextFormField(
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Password is required';
                                }
                              },
                              onChanged: (value) {
                                setState(() {
                                  password = value;
                                });
                              },
                              obscureText: isVisible,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14.4,
                                  height: 2.0),
                              decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        isVisible = !isVisible;
                                      });
                                    },
                                    icon: Icon(isVisible
                                        ? Icons.visibility_off
                                        : Icons.visibility),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white,
                                  contentPadding: const EdgeInsets.only(
                                      left: 16.0,
                                      bottom: 8.0,
                                      top: 8.0,
                                      right: 16.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(0XFFdee2e6),
                                          style: BorderStyle.solid,
                                          width: 1.0)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(0XFFfed098),
                                          style: BorderStyle.solid,
                                          width: 2.0)),
                                  errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.0, color: Colors.red)),
                                  errorStyle:
                                      TextStyle(fontWeight: FontWeight.bold),
                                  focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.0, color: Colors.red)),
                                  labelText: 'password'),
                              keyboardType: TextInputType.text,
                            ),
                            SizedBox(
                              height: 60,
                            ),
                            TextButton(
                              onPressed: () {
                                if (isLoggingIn) {
                                  return null;
                                } else {
                                  if (_formKey.currentState!.validate()) {
                                    _login();
                                  }
                                }
                              },
                              child: Text(
                                'Sign In',
                                style: TextStyle(
                                    color: Colors.black,
                                    letterSpacing: 2.0,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold),
                              ),
                              style: ButtonStyle(
                                  backgroundColor: isLoggingIn
                                      ? MaterialStateProperty.all(
                                          Colors.orange[300])
                                      : MaterialStateProperty.all(
                                          Color(0XFFf6891f)),
                                  textStyle: MaterialStateProperty.all(
                                      TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Color(0XFF212529))),
                                  padding: MaterialStateProperty.all(
                                      EdgeInsets.fromLTRB(
                                          52, 11.2, 52.0, 11.2))),
                            ),
                            Container(
                              child: isLoggingIn
                                  ? CircularProgressIndicator(
                                      color: Color(0XFFf6891f),
                                      strokeWidth: 2.0,
                                    )
                                  : null,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
          ),
        ));
  }
}
