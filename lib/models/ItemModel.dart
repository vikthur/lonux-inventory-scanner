class ItemModel {
  int itemStock;
  String itemCost;
  String itemName;
  String itemSoldIn;
  String itemCategory;
  dynamic image1;

  ItemModel(
      {required this.itemStock,
      required this.itemCost,
      required this.itemName,
      required this.itemSoldIn,
      required this.itemCategory});
}
