class ImageModel {
  String imageType = 'original';
  dynamic imageSrc;

  ImageModel({required this.imageSrc, required this.imageType});
}
