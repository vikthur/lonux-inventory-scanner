import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Shop {
  getShopData() async {
    var localStorage = await SharedPreferences.getInstance();
    var shopJson = localStorage.getString('shop');
    var shop = jsonDecode(shopJson!);

    return shop;
  }
}
