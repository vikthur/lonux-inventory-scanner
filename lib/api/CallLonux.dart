import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class CallLonux {
  final String _baseUrl = 'https://manager.lonux.com.ng/api/v1/m/';
  String _token = '';

  postData(endpoint, data) async {
    var url = Uri.parse(_baseUrl + endpoint);
    var response = await http.post(url,
        body: jsonEncode(data), headers: await _getHeaders());

    return response;
  }

  getData(endpoint) async {
    var url = Uri.parse(_baseUrl + endpoint);
    var response = await http.get(url, headers: await _getHeaders());

    return response;
  }

  _getHeaders() async {
    return {
      'Accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': 'Bearer ' + await _getToken()
    };
  }

  _getToken() async {
    var localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');

    if (token == null) {
      return '';
    }
    return token;
  }
}
