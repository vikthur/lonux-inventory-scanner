import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lonux_inventory_scanner/Wrapper.dart';
import 'package:lonux_inventory_scanner/api/CallLonux.dart';
import 'package:lonux_inventory_scanner/auth/Logout.dart';
import 'package:lonux_inventory_scanner/models/ImageModel.dart';
import 'package:lonux_inventory_scanner/widgets/ImageCard.dart';

class AddItem extends StatefulWidget {
  const AddItem({Key? key}) : super(key: key);

  @override
  _AddItemState createState() => _AddItemState();
}

class _AddItemState extends State<AddItem> {
  Map _item = {};
  final Logout logoutClass = Logout();
  // List<dynamic> images = [];
  List<dynamic> imagesList = [];
  bool firstCall = true;

  final _formKey = GlobalKey<FormState>();
  dynamic itemName,
      itemCost,
      itemStock,
      itemSoldIn,
      itemCategory,
      itemDesc,
      isSaving = false,
      image1,
      image2,
      image3,
      image4,
      image5;
  Map data = {};

  List listItems = ['retail', 'wholesale'];

  _saveItem() async {
    setState(() {
      isSaving = true;
    });

    pocessImages();

    data['item_cost'] = itemCost;
    data['item_stock'] = itemStock;
    data['item_sold_in'] = itemSoldIn;
    data['item_category'] = itemCategory;
    data['item_desc'] = itemDesc;
    data['item_name'] = itemName;

    print(data);

    Response res = await CallLonux().postData('inventory_store', data);

    var body = res.body;

    print(body);
    print(res.statusCode);

    setState(() {
      isSaving = true;
    });
  }

  pocessImages() async {
    for (var i = 0; i < imagesList.length; i++) {
      var index = i + 1;
      if (imagesList[i].imageType != 'placeholder') {
        data['image' + index.toString()] = imagesList[i].imageType == 'url'
            ? imagesList[i].imageSrc
            : 'data:image/png;base64,' +
                base64Encode(File(imagesList[i].imageSrc).readAsBytesSync());
      }
    }
  }

  Future<void> openDialoge() async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Icon(
              Icons.warning,
              color: Color(0XFFdc3545),
              size: 36.0,
            ),
            // Text('Logout ?', style: TextStyle(fontWeight: FontWeight.bold)),
            content: Text('Are you sure you want to logout ?',
                style: TextStyle(fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('No',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.green))),
              TextButton(
                  onPressed: () => _logoutUser(),
                  child: Text('Yes',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0XFFdc3545))))
            ],
          );
        });
  }

  void _logoutUser() async {
    await logoutClass.logout();
    // Navigator.pushReplacementNamed(context, '/');
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => Wrapper()),
      (Route<dynamic> route) => false,
    );
  }

  void initializeImages() {
    var numOfImages = _item['images'].length;
    for (var i = 0; i < numOfImages; i++) {
      imagesList
          .add(ImageModel(imageType: 'url', imageSrc: _item['images'][i]));
    }
  }

  setImages(imagesCount) {
    const maxNumImages = 5;
    var numOfImages = imagesCount;
    List placeholderArr = [];
    num remainingImageFileds = maxNumImages - numOfImages;
    print(
        'number of images $numOfImages , number of remaining fields: $remainingImageFileds');
    for (var i = 0; i < remainingImageFileds; i++) {
      placeholderArr.add(ImageModel(
          imageType: 'placeholder', imageSrc: 'assets/images/addImage.png'));
    }

    setState(() {
      imagesList = imagesList + placeholderArr;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    _item = ModalRoute.of(context)!.settings.arguments as Map;
    // images = _item['images'];
    itemName = _item['item_name'];
    itemCost = _item['item_cost'];
    itemCategory = _item['category']['item'];

    if (firstCall) {
      initializeImages();
      setImages(_item['images'].length);
      setState(() {
        firstCall = false;
      });
    }

    print(_item);
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.grey[200],
        elevation: 0.0,
        title: Row(
          children: [
            Image.asset(
              'assets/images/lonux-icon.png',
              width: 32.0,
            ),
            SizedBox(
              width: 3.0,
            ),
            Text(
              '| Dashboard',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
            ),
          ], // children
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 20.0, 16.0, 0.0),
            child: GestureDetector(
              onTap: () => openDialoge(),
              child: Text(
                'Logout',
                style: TextStyle(
                    color: Color(0XFFdc3545), fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
          child: Column(
            children: [
              Text(
                'Images for ${_item['item_name']}',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'MontserratMedium'),
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0),
                child: Container(
                  height: 250.0,
                  child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: imagesList
                          .map<Widget>((image) => ImageCard(
                                imageObj: image,
                                delete: () {
                                  setState(() {
                                    imagesList.remove(image);
                                  });
                                  // setImages(imagesList.length);
                                  print(image['imageType']);
                                },
                              ))
                          .toList()),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Item Name',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value == null) {
                            return 'Item must have a name';
                          }
                        },
                        onChanged: (value) {
                          setState(() {
                            itemName = value;
                          });
                        },
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.4,
                            height: 2.0),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            contentPadding: const EdgeInsets.only(
                                left: 8.0, bottom: 8.0, top: 8.0, right: 8.0),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFdee2e6),
                                    style: BorderStyle.solid,
                                    width: 1.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFfed098),
                                    style: BorderStyle.solid,
                                    width: 2.0)),
                            errorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            errorStyle: TextStyle(fontWeight: FontWeight.bold),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            labelText: 'item name'),
                        initialValue: itemName,
                        keyboardType: TextInputType.text,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Item Price',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value == null) {
                            return 'Item must have a price';
                          }
                        },
                        onChanged: (value) {
                          setState(() {
                            itemCost = value;
                          });
                        },
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.4,
                            height: 2.0),
                        decoration: InputDecoration(
                            prefix: Padding(
                              padding: EdgeInsets.only(right: 5),
                              child: Text('NGN'),
                            ),
                            prefixStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            contentPadding: const EdgeInsets.only(
                                left: 8.0, bottom: 8.0, top: 8.0, right: 8.0),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFdee2e6),
                                    style: BorderStyle.solid,
                                    width: 1.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFfed098),
                                    style: BorderStyle.solid,
                                    width: 2.0)),
                            errorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            errorStyle: TextStyle(fontWeight: FontWeight.bold),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            labelText: 'item price'),
                        initialValue: itemCost.toString(),
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Item Category',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value == null) {
                            return 'Item must have a category';
                          }
                        },
                        onChanged: (value) {
                          setState(() {
                            itemCategory = value;
                          });
                        },
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.4,
                            height: 2.0),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            contentPadding: const EdgeInsets.only(
                                left: 8.0, bottom: 8.0, top: 8.0, right: 8.0),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFdee2e6),
                                    style: BorderStyle.solid,
                                    width: 1.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFfed098),
                                    style: BorderStyle.solid,
                                    width: 2.0)),
                            errorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            errorStyle: TextStyle(fontWeight: FontWeight.bold),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            labelText: 'item category'),
                        initialValue: itemCategory,
                        keyboardType: TextInputType.text,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Item Sold In',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border:
                                Border.all(width: 1, color: Color(0XFFdee2e6))),
                        child: Padding(
                          padding:
                              const EdgeInsets.only(left: 12.0, right: 12.0),
                          child: DropdownButton(
                            onChanged: (value) {
                              setState(() {
                                itemSoldIn = value;
                              });
                            },
                            hint: Text('Item sold in'),
                            icon: Icon(Icons.arrow_downward),
                            isExpanded: true,
                            value: itemSoldIn,
                            underline: SizedBox(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontFamily: 'Montserrat',
                                fontSize: 14.4,
                                height: 2.0),
                            items: listItems.map((item) {
                              return DropdownMenuItem(
                                child: Text(item),
                                value: item,
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Item Quantity',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value == null) {
                            return 'How many of this item do you have ?';
                          }
                        },
                        onChanged: (value) {
                          setState(() {
                            itemStock = value;
                          });
                        },
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.4,
                            height: 2.0),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            contentPadding: const EdgeInsets.only(
                                left: 8.0, bottom: 8.0, top: 8.0, right: 8.0),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFdee2e6),
                                    style: BorderStyle.solid,
                                    width: 1.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFfed098),
                                    style: BorderStyle.solid,
                                    width: 2.0)),
                            errorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            errorStyle: TextStyle(fontWeight: FontWeight.bold),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            labelText: 'item stock'),
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Note to Buyer (optional)',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      TextFormField(
                        onChanged: (value) {
                          setState(() {
                            itemDesc = value;
                          });
                        },
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.4,
                            height: 2.0),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            contentPadding: const EdgeInsets.only(
                                left: 8.0, bottom: 8.0, top: 8.0, right: 8.0),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFdee2e6),
                                    style: BorderStyle.solid,
                                    width: 1.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFFfed098),
                                    style: BorderStyle.solid,
                                    width: 2.0)),
                            errorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            errorStyle: TextStyle(fontWeight: FontWeight.bold),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1.0, color: Colors.red)),
                            labelText: 'e.g weight, item, condition and etc'),
                        initialValue: itemDesc,
                        keyboardType: TextInputType.multiline,
                        minLines: 3,
                        maxLength: 100,
                        maxLines: null,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Center(
                        child: TextButton(
                          onPressed: () {
                            if (isSaving) {
                              return null;
                            } else {
                              if (_formKey.currentState!.validate()) {
                                _saveItem();
                              }
                            }
                          },
                          child: Text(
                            'Add Item',
                            style: TextStyle(
                                color: Colors.black,
                                letterSpacing: 2.0,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold),
                          ),
                          style: ButtonStyle(
                              backgroundColor: isSaving
                                  ? MaterialStateProperty.all(
                                      Colors.orange[300])
                                  : MaterialStateProperty.all(
                                      Color(0XFFf6891f)),
                              textStyle: MaterialStateProperty.all(TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0XFF212529))),
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.fromLTRB(52, 11.2, 52.0, 11.2))),
                        ),
                      ),
                      Center(
                        child: Container(
                          child: isSaving
                              ? CircularProgressIndicator(
                                  color: Color(0XFFf6891f),
                                  strokeWidth: 2.0,
                                )
                              : null,
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
