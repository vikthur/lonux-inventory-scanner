import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:lonux_inventory_scanner/Wrapper.dart';
import 'package:lonux_inventory_scanner/api/CallLonux.dart';
import 'package:lonux_inventory_scanner/auth/Logout.dart';
import 'package:lonux_inventory_scanner/models/Shop.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final Logout logoutClass = Logout();
  Map _shop = {};
  String infoText = '';
  bool isScanning = false;
  bool notFound = false;
  bool addManually = false;
  // get shop
  _getShop() async {
    final shop = Shop();
    var shopData = await shop.getShopData();
    setState(() {
      _shop = shopData;
    });
    // print(shopData);
  }

  // logout dialogue
  Future<void> openDialoge() async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Icon(
              Icons.warning,
              color: Color(0XFFdc3545),
              size: 36.0,
            ),
            // Text('Logout ?', style: TextStyle(fontWeight: FontWeight.bold)),
            content: Text('Are you sure you want to logout ?',
                style: TextStyle(fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('No',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.green))),
              TextButton(
                  onPressed: () => _logoutUser(),
                  child: Text('Yes',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0XFFdc3545))))
            ],
          );
        });
  }

  void _logoutUser() async {
    await logoutClass.logout();
    // Navigator.pushReplacementNamed(context, '/');
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => Wrapper()),
      (Route<dynamic> route) => false,
    );
  }

  _addManually() {
    Navigator.pushNamed(context, '/add', arguments: null);
  }

  // scanner
  Future<void> _scanBarCode() async {
    String scanResult = '';
    setState(() {
      isScanning = true;
    });

    try {
      scanResult = await FlutterBarcodeScanner.scanBarcode(
          '#f6891f', 'Cancel', true, ScanMode.BARCODE);
      print(scanResult);
    } on PlatformException {
      setState(() {
        infoText = 'Failed to get platform version.';
      });
    }

    if (!mounted) return;
    setState(() {
      infoText = 'Getting item data...';
    });

    getItemData(scanResult);

    print('printing result again: $scanResult');
  }

  getItemData(String upc) async {
    Response res = await CallLonux().postData('upc_check', {'code': upc});
    Map body = jsonDecode(res.body);
    print('request body is: $body and status code is ${res.statusCode}');
    setState(() {
      isScanning = false;
      infoText = '';
    });
    if (body['success']) {
      Navigator.pushNamed(context, '/add', arguments: body['data']);
    } else if (res.statusCode == 404) {
      setState(() {
        infoText =
            'Lonux does not have this item at this time, be the first to add it.';
        addManually = true;
      });
    } else if (res.statusCode == 403) {
      setState(() {
        infoText =
            'You shop already have this item. Manage it on your web dashboard';
      });
    } else if (res.statusCode == 401) {
      _logoutUser();
      Navigator.pushReplacementNamed(context, '/login',
          arguments: {'mes': 'You have been logged out. Login again'});
    }
  }

  @override
  void initState() {
    super.initState();
    _getShop();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.grey[200],
          elevation: 0.0,
          title: Row(
            children: [
              Image.asset(
                'assets/images/lonux-icon.png',
                width: 32.0,
              ),
              SizedBox(
                width: 3.0,
              ),
              Text(
                '| Dashboard',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
            ], // children
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 20.0, 16.0, 0.0),
              child: GestureDetector(
                onTap: () => openDialoge(),
                child: Text(
                  'Logout',
                  style: TextStyle(
                      color: Color(0XFFdc3545), fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(15.0, 60.0, 15.0, 110.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Card(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16.0, 18.0, 16.0, 18.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Center(
                          child: Text('Shop Name',
                              style: TextStyle(
                                  fontWeight: FontWeight.w200,
                                  fontSize: 18.0,
                                  fontFamily: 'MontserratMedium',
                                  color: Color(0XFF212529))),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: Text('${_shop['shop_name']}',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14.4,
                                  fontFamily: 'Montserrat',
                                  color: Color(0XFF212529))),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Center(
                          child: Text('Shop Address',
                              style: TextStyle(
                                  fontWeight: FontWeight.w200,
                                  fontSize: 18.0,
                                  fontFamily: 'MontserratMedium',
                                  color: Color(0XFF212529))),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: Text('${_shop['shop_address']}',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14.4,
                                  fontFamily: 'Montserrat',
                                  color: Color(0XFF212529))),
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  children: [
                    Container(
                      child: isScanning
                          ? CircularProgressIndicator(
                              color: Color(0XFFf6891f),
                              strokeWidth: 2.0,
                            )
                          : null,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      infoText,
                      style: TextStyle(
                          color: notFound
                              ? Colors.redAccent[700]
                              : Colors.blue[900],
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: addManually
                          ? TextButton(
                              onPressed: () => _addManually(),
                              child: Text(
                                'Add Manually',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'MontserratMedium'),
                              ))
                          : null,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => _scanBarCode(),
          backgroundColor: Color(0XFFf6891f),
          child: Icon(
            Icons.crop_free,
            color: Colors.white,
          ),
        ));
  }
}
